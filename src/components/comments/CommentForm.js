import React, { PureComponent } from "react";
import { connect } from "react-redux";

class CommentForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      commentForm: {
        name: "",
        content: "",
        comment: "",
      },
    };
  }

  commentHandleChange = (e) => {
    const { id, value } = e.target;
    const { commentForm } = this.state;

    this.setState({
      commentForm: {
        ...commentForm,
        [id]: value,
      },
    });
  };

  commentHandleSave = (e) => {
    e.preventDefault();
    const {
      commentForm: { name, content, comment },
    } = this.state;
    const { addComment } = this.props;

    addComment({
      name,
      content,
      comment,
      editing: false,
    });

    this.setState({
      commentForm: {
        name,
        content,
        comment: "",
      },
    });
  };
  render() {
    const {
      commentForm: { comment },
    } = this.state;
    const { users, posts } = this.props;

    return (
      <div className="mb-3">
        <form onSubmit={this.commentHandleSave}>
          <div className="row form-group">
            <div className="col-12 mb-3">
              <select
                id="name"
                onChange={this.commentHandleChange}
                className="form-control mb-3 col-md-3"
              >
                <option selected disabled>
                  Select Comment Owner
                </option>
                {users.map(({ userId, name }) => (
                  <option key={userId} value={name}>
                    {name}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-12 mb-3">
              <select
                id="content"
                onChange={this.commentHandleChange}
                className="form-control mb-3 col-md-3"
              >
                <option selected disabled>
                  Select Post to Comment
                </option>
                {posts.map(({ postId, content }) => (
                  <option key={postId} value={content}>
                    {content}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-12 mb-3">
              <div className="form-group">
                <label>Comment</label>
                <textarea
                  className="form-control"
                  id="comment"
                  rows="3"
                  placeholder="Enter comment"
                  value={comment}
                  onChange={this.commentHandleChange}
                  required
                />
              </div>
            </div>
          </div>
          <button className="btn btn-primary">Comment</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = ({ users, posts }) => {
  return {
    users,
    posts,
  };
};

const mapActionToProps = (dispatch) => {
  return {
    addComment: (payload) =>
      dispatch({
        type: "ADD_COMMENT",
        payload,
      }),
  };
};

export default connect(mapStateToProps, mapActionToProps)(CommentForm);
