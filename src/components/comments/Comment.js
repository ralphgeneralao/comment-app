import React from "react";

const Comment = ({ postComment, dispatch }) => {
  const { id, name, content, comment } = postComment;

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">{content}</h5>
        <h6 className="card-subtitle mb-2">
          <strong>Comment Owner: </strong>
          {name}
        </h6>
        <h6 className="card-subtitle mb-2">
          <strong>Comment: </strong>
          {comment}
        </h6>
        <div>
          <span
            className="mx-2 text-success"
            onClick={() =>
              dispatch({
                type: "EDIT_COMMENT",
                payload: id,
              })
            }
          >
            <i className="fas fa-pen"></i>
          </span>
          <span
            className="mx-2 text-danger"
            onClick={() =>
              dispatch({
                type: "DELETE_COMMENT",
                payload: id,
              })
            }
          >
            <i className="fas fa-trash"></i>
          </span>
        </div>
      </div>
    </div>
  );
};

export default Comment;
