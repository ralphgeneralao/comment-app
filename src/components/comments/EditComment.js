import React, { PureComponent } from "react";

class EditComment extends PureComponent {
  commentHandleEdit = (e) => {
    e.preventDefault();

    const newComment = this.getNewComment.value;

    const { dispatch, postComment } = this.props;
    const { id } = postComment;

    const data = {
      newComment,
    };

    dispatch({
      type: "UPDATE_COMMENT",
      id,
      data,
    });
  };
  render() {
    const { postComment, users, posts } = this.props;
    const { name, comment, content } = postComment;

    return (
      <div className="mb-3">
        <form onSubmit={this.commentHandleEdit}>
          <div className="row form-group">
            <div className="col-12 mb-3">
              <select
                id="name"
                disabled
                defaultValue={name}
                className="form-control mb-3 col-md-3"
              >
                {users.map(({ userId, name }) => (
                  <option key={userId} value={name}>
                    {name}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-12 mb-3">
              <select
                id="content"
                disabled
                defaultValue={content}
                className="form-control mb-3 col-md-3"
              >
                {posts.map(({ postId, content }) => (
                  <option key={postId} value={content}>
                    {content}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-12 mb-3">
              <div className="form-group">
                <label>Comment</label>
                <textarea
                  className="form-control"
                  id="comment"
                  rows="3"
                  placeholder="Enter updated comment"
                  ref={(input) => (this.getNewComment = input)}
                  defaultValue={comment}
                  required
                />
              </div>
            </div>
          </div>
          <button className="btn btn-success">Update Comment</button>
        </form>
      </div>
    );
  }
}

export default EditComment;
