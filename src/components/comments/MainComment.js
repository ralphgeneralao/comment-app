import React from "react";

import CommentForm from "./CommentForm";
import AllComments from "./AllComments";

const MainComment = () => {
  return (
    <div className="container">
      <CommentForm />

      <AllComments />
    </div>
  );
};

export default MainComment;
