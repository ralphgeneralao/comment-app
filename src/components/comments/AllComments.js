import React from "react";
import { connect } from "react-redux";

import Comment from "./Comment";
import EditComment from "../comments/EditComment";

const AllComments = ({ postComments, users, posts, dispatch }) => {
  return (
    <div>
      {postComments.length > 0 ? (
        postComments.map((postComment) => {
          const { id, editing } = postComment;
          return (
            <div key={id}>
              {editing ? (
                <EditComment
                  key={id}
                  postComment={postComment}
                  users={users}
                  posts={posts}
                  dispatch={dispatch}
                />
              ) : (
                <Comment
                  key={id}
                  postComment={postComment}
                  dispatch={dispatch}
                />
              )}
            </div>
          );
        })
      ) : (
        <h6 className="card-subtitle mb-2">
          <strong>
            <em>No Comments Found</em>
          </strong>
        </h6>
      )}
    </div>
  );
};

const mapStateToProps = ({ users, posts, comments }) => {
  return {
    users,
    posts,
    postComments: comments,
  };
};

export default connect(mapStateToProps)(AllComments);
