import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Link } from "react-router-dom";

import MainPage from "./users/MainPage";
import MainPost from "./posts/MainPost";
import MainComment from "./comments/MainComment";
import All from "./all/All";

const Homepage = () => {
  return (
    <div>
      <Router>
        <div>
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary mb-3">
            <div className="container">
              <ul className="navbar-nav mr-auto" id="navbarSupportedContent">
                <li className="nav-item">
                  <Link to="/" className="nav-link text-white">
                    Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/users" className="nav-link text-white">
                    Users
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/posts" className="nav-link text-white">
                    Posts
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/comments" className="nav-link text-white">
                    Comments
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/all" className="nav-link text-white">
                    All
                  </Link>
                </li>
              </ul>
            </div>
          </nav>
          <Switch>
            <Route exact path="/" />
            <Route path="/users">
              <MainPage />
            </Route>
            <Route path="/posts">
              <MainPost />
            </Route>
            <Route path="/comments">
              <MainComment />
            </Route>
            <Route path="/all">
              <All />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
};

export default Homepage;
