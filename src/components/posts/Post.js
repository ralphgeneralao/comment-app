import React from "react";

const Post = (props) => {
  const { dispatch, post } = props;
  const { postId, content, name, date } = post;

  return (
    <div className="card mb-3">
      <div className="card-body">
        <h5 className="card-title">{content}</h5>
        <h6 className="card-subtitle mb-2">
          <strong>Post Owner: </strong>
          {name}
        </h6>
        <h6 className="card-subtitle mb-2">
          <strong>Date Posted: </strong>
          {date}
        </h6>
        <div>
          <span
            className="mx-2 text-success"
            onClick={() =>
              dispatch({
                type: "EDIT_POST",
                payload: postId,
              })
            }
          >
            <i className="fas fa-pen"></i>
          </span>
          <span
            className="mx-2 text-danger"
            onClick={() =>
              dispatch({
                type: "DELETE_POST",
                payload: postId,
              })
            }
          >
            <i className="fas fa-trash"></i>
          </span>
        </div>
      </div>
    </div>
  );
};

export default Post;
