import React from "react";
import { connect } from "react-redux";

import Post from "./Post";
import EditPost from "./EditPost";

const AllPosts = ({ posts, dispatch, users }) => {
  return (
    <div>
      {posts.length > 0 ? (
        posts.map((post) => {
          const { postId, editing } = post;
          return (
            <div key={postId}>
              {editing ? (
                <EditPost
                  key={postId}
                  post={post}
                  users={users}
                  dispatch={dispatch}
                />
              ) : (
                <Post key={postId} post={post} dispatch={dispatch} />
              )}
            </div>
          );
        })
      ) : (
        <h6 className="card-subtitle mb-2">
          <strong>
            <em>No Post Found</em>
          </strong>
        </h6>
      )}
    </div>
  );
};

const mapStateToProps = ({ users, posts }) => {
  return {
    users,
    posts,
  };
};

export default connect(mapStateToProps)(AllPosts);
