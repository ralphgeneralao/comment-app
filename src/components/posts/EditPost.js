import React, { PureComponent } from "react";

class EditPost extends PureComponent {
  postHandleEdit = (e) => {
    e.preventDefault();

    const newContent = this.getContent.value;

    let newDate = new Date();
    let month = newDate.getMonth() + 1;
    let day = newDate.getDate();
    let year = newDate.getFullYear();

    let updatedDate = `${month} - ${day} - ${year}`;

    const {
      dispatch,
      post: { postId },
    } = this.props;

    const data = {
      newContent,
      updatedDate,
    };

    dispatch({
      type: "UPDATE_POST",
      postId,
      data,
    });
  };
  render() {
    const { users, post } = this.props;
    const { name, content } = post;

    return (
      <div className="mb-3">
        <form onSubmit={this.postHandleEdit}>
          <div className="row form-group">
            <div className="col-12 mb-3">
              <select
                id="name"
                disabled
                defaultValue={name}
                className="form-control mb-3 col-md-3"
              >
                {users.map(({ userId, name }) => (
                  <option key={userId} value={name}>
                    {name}
                  </option>
                ))}
              </select>
              <div className="form-group">
                <label>Post</label>
                <textarea
                  className="form-control"
                  id="content"
                  rows="3"
                  placeholder="Enter Updated Post"
                  ref={(input) => (this.getContent = input)}
                  defaultValue={content}
                  required
                />
              </div>
            </div>
          </div>
          <button className="btn btn-success">Update</button>
        </form>
      </div>
    );
  }
}

export default EditPost;
