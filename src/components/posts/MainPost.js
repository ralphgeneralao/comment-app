import React from "react";

import PostForm from "./PostForm";
import AllPosts from "./AllPosts";

const MainPost = () => {
  return (
    <div className="container">
      <PostForm />
      <AllPosts />
    </div>
  );
};

export default MainPost;
