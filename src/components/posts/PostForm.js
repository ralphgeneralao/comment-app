import React, { PureComponent } from "react";
import { connect } from "react-redux";

class PostForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      postForm: {
        content: "",
        name: "",
        userId: "",
      },
    };
  }

  postHandleChange = (e) => {
    const { id, value } = e.target;
    const { postForm } = this.state;

    this.setState({
      postForm: {
        ...postForm,
        [id]: value,
      },
    });
  };

  postHandleSave = (e) => {
    e.preventDefault();

    const {
      postForm: { name, content },
    } = this.state;
    const { addPost, posts } = this.props;

    const postExist = posts.find((item) => item.content === content);

    let newDate = new Date();
    let month = newDate.getMonth() + 1;
    let day = newDate.getDate();
    let year = newDate.getFullYear();

    let date = `${month} - ${day} - ${year}`;

    if (!postExist) {
      addPost({
        name,
        content,
        date,
        editing: false,
      });
    } else {
      alert("post already exist");
    }

    this.setState({
      postForm: {
        content: "",
        name,
        userId: "",
      },
    });
  };

  render() {
    const { users } = this.props;
    const {
      postForm: { content },
    } = this.state;

    return (
      <div className="mb-3">
        <form onSubmit={this.postHandleSave}>
          <div className="row form-group">
            <div className="col-12 mb-3">
              <select
                id="name"
                onChange={this.postHandleChange}
                className="form-control mb-3 col-md-3"
              >
                <option selected disabled>
                  Select Post Owner
                </option>
                {users.map(({ userId, name }) => {
                  return (
                    <option key={userId} value={name}>
                      {name}
                    </option>
                  );
                })}
              </select>
              <div className="form-group">
                <label>Post</label>
                <textarea
                  className="form-control"
                  id="content"
                  rows="3"
                  placeholder="Enter Post"
                  value={content}
                  onChange={this.postHandleChange}
                  required
                />
              </div>
            </div>
          </div>
          <button className="btn btn-primary">Add</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = ({ users, posts }) => {
  return {
    users,
    posts,
  };
};

const mapActionToProps = (dispatch) => {
  return {
    addPost: (payload) =>
      dispatch({
        type: "ADD_POST",
        payload,
      }),
  };
};

export default connect(mapStateToProps, mapActionToProps)(PostForm);
