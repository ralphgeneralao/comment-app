import React from "react";
import { connect } from "react-redux";

import GroupPost from "./GroupPost";

const All = ({ users, posts, postComments }) => {
  const AllComponents = users.map((user) => {
    const groupedPosts = posts.filter(({ name }) => name === user.name);
    return {
      ...user,
      groupedPosts,
    };
  });

  return (
    <div className="container">
      {AllComponents.length > 0 ? (
        AllComponents.map(({ userId, name, groupedPosts }) => {
          return (
            <div className="card" key={userId}>
              <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <GroupPost
                  groupedPosts={groupedPosts}
                  postComments={postComments}
                />
              </div>
            </div>
          );
        })
      ) : (
        <h2>
          <em>Nothing to show here</em>
        </h2>
      )}
    </div>
  );
};

const mapStateToProps = ({ users, posts, comments }) => {
  return {
    users,
    posts,
    postComments: comments,
  };
};

export default connect(mapStateToProps)(All);
