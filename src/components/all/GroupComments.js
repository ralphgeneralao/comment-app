import React from "react";

const GroupComments = ({ sameComments }) => {
  return (
    <div>
      <h6 className="card-subtitle mb-2">
        <strong>
          <span className="mr-4"></span>Comments:{" "}
        </strong>
      </h6>
      {sameComments.length > 0 ? (
        sameComments.map(({ id, comment, name }) => {
          return (
            <div key={id}>
              <h6 className="card-subtitle mb-2">
                <span className="mx-2 text-success">
                  <i className="fas fa-comment-dots"></i>
                </span>
                "{comment}" <em>commented by</em> {name}
              </h6>
            </div>
          );
        })
      ) : (
        <p>
          <em>No comments for this post</em>
        </p>
      )}
    </div>
  );
};

export default GroupComments;
