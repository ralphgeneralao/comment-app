import React from "react";

import GroupComments from "./GroupComments";

const GroupPost = ({ groupedPosts, postComments }) => {
  const groupedComments = groupedPosts.map((groupedPost) => {
    const sameComments = postComments.filter(
      (postComment) => postComment.content === groupedPost.content
    );
    return {
      ...groupedPost,
      sameComments,
    };
  });

  return (
    <div>
      <h6 className="card-subtitle mb-2">
        <strong>Posts: </strong>
      </h6>
      {groupedComments.length > 0 ? (
        groupedComments.map(({ postId, content, sameComments }) => {
          return (
            <div key={postId}>
              <h6 className="card-subtitle mb-2">
                <span className="mx-2 text-primary">
                  <i className="fas fa-book"></i>
                </span>{" "}
                {content}
              </h6>
              <GroupComments sameComments={sameComments} />
            </div>
          );
        })
      ) : (
        <p>
          <em>No Post for this user</em>
        </p>
      )}
    </div>
  );
};

export default GroupPost;
