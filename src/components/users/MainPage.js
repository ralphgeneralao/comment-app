import React from "react";

import UserForm from "./UserForm";
import AllUsers from "./AllUsers";

const MainPage = () => {
  return (
    <div className="container">
      <UserForm />
      <AllUsers />
    </div>
  );
};

export default MainPage;
