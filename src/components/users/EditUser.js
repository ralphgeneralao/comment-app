import React, { PureComponent } from "react";

class EditUser extends PureComponent {
  handleUpdate = (e) => {
    e.preventDefault();

    const newName = this.getName.value;
    const newUserName = this.getUserName.value;
    const newEmail = this.getEmail.value;
    const newPhone = this.getPhone.value;

    const {
      user: { userId },
      dispatch,
    } = this.props;

    const data = {
      newName,
      newUserName,
      newEmail,
      newPhone,
    };

    dispatch({
      type: "UPDATE_USER",
      userId,
      data,
    });
  };

  render() {
    const { user } = this.props;
    const { name, userName, email, phone } = user;

    return (
      <div className="mb-3">
        <form onSubmit={this.handleUpdate}>
          <div className="row form-group">
            <div className="col">
              <input
                id="name"
                className="form-control"
                placeholder="Enter updated name"
                ref={(input) => (this.getName = input)}
                defaultValue={name}
                required
              />
            </div>
            <div className="col">
              <input
                id="userName"
                className="form-control"
                placeholder="Enter updated username"
                ref={(input) => (this.getUserName = input)}
                defaultValue={userName}
                required
              />
            </div>
          </div>
          <div className="row form-group">
            <div className="col">
              <input
                id="email"
                className="form-control"
                placeholder="Enter updated email"
                ref={(input) => (this.getEmail = input)}
                defaultValue={email}
                required
              />
            </div>
            <div className="col">
              <input
                id="phone"
                className="form-control"
                placeholder="Enter updated phone"
                ref={(input) => (this.getPhone = input)}
                defaultValue={phone}
                required
              />
            </div>
          </div>
          <button className="btn btn-success">Update</button>
        </form>
      </div>
    );
  }
}

export default EditUser;
