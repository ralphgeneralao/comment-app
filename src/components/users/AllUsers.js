import React from "react";
import { connect } from "react-redux";

import EditUser from "./EditUser";
import User from "./User";

const AllUsers = ({ users, dispatch }) => {
  return (
    <div>
      {users.length > 0 ? (
        users.map((user) => {
          const { userId, editing } = user;

          return (
            <div key={userId}>
              {editing ? (
                <EditUser key={userId} user={user} dispatch={dispatch} />
              ) : (
                <User key={userId} user={user} dispatch={dispatch} />
              )}
            </div>
          );
        })
      ) : (
        <h6 className="card-subtitle mb-2">
          <strong>
            <em>No Users Found</em>
          </strong>
        </h6>
      )}
    </div>
  );
};

const mapStateToProps = ({ users }) => ({ users });

export default connect(mapStateToProps)(AllUsers);
