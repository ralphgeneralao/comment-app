import React from "react";

const User = ({ user, dispatch }) => {
  const { userId, name, userName, email, phone } = user;

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <h6 className="card-subtitle mb-2">
          <strong>User Name: </strong>
          {userName}
        </h6>
        <h6 className="card-subtitle mb-2">
          <strong>Email: </strong>
          {email}
        </h6>
        <h6 className="card-subtitle mb-2">
          <strong>Phone #: </strong>
          {phone}
        </h6>
        <div>
          <span
            className="mx-2 text-success"
            onClick={() =>
              dispatch({
                type: "EDIT_USER",
                payload: userId,
              })
            }
          >
            <i className="fas fa-pen"></i>
          </span>
          <span
            className="mx-2 text-danger"
            onClick={() =>
              dispatch({
                type: "DELETE_USER",
                payload: userId,
              })
            }
          >
            <i className="fas fa-trash"></i>
          </span>
        </div>
      </div>
    </div>
  );
};

export default User;
