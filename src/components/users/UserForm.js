import React, { PureComponent } from "react";
import { connect } from "react-redux";

class UserForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      userForm: {
        name: "",
        userName: "",
        email: "",
        phone: "",
      },
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { userForm } = this.state;
    const { name, userName, email, phone } = userForm;
    const { addUser, users } = this.props;

    const userExist = users.find((user) => user.name === name);

    if (!userExist) {
      addUser({
        name,
        userName,
        email,
        phone,
        editing: false,
      });
    }

    this.setState({
      userForm: {
        name: "",
        userName: "",
        email: "",
        phone: "",
      },
    });
  };

  handleChange = (e) => {
    const { id, value } = e.target;
    const { userForm } = this.state;

    this.setState({
      userForm: {
        ...userForm,
        [id]: value,
      },
    });
  };

  render() {
    const { userForm } = this.state;
    const { name, userName, email, phone } = userForm;

    return (
      <div className="mb-3">
        <form onSubmit={this.handleSubmit}>
          <div className="row form-group">
            <div className="col">
              <input
                id="name"
                type="text"
                className="form-control"
                placeholder="Enter name"
                value={name}
                onChange={this.handleChange}
                required
              />
            </div>
            <div className="col">
              <input
                id="userName"
                type="text"
                className="form-control"
                placeholder="Enter Username"
                value={userName}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>
          <div className="row form-group">
            <div className="col">
              <input
                id="email"
                type="email"
                className="form-control"
                placeholder="Enter Email"
                value={email}
                onChange={this.handleChange}
                required
              />
            </div>
            <div className="col">
              <input
                id="phone"
                type="phone"
                className="form-control"
                placeholder="Enter Phone"
                value={phone}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>
          <button className="btn btn-primary">Add</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = ({ users }) => ({ users });

const mapActionToProps = (dispatch) => {
  return {
    addUser: (payload) =>
      dispatch({
        type: "ADD_USER",
        payload,
      }),
  };
};

export default connect(mapStateToProps, mapActionToProps)(UserForm);
