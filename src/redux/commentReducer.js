const initialState = [];

const commentReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_COMMENT":
      return [
        ...state,
        {
          id: new Date().toISOString(),
          ...action.payload,
        },
      ];

    case "EDIT_COMMENT":
      return [
        ...state.map((comment) =>
          comment.id === action.payload
            ? {
                ...comment,
                editing: !comment.editing,
              }
            : comment
        ),
      ];

    case "DELETE_COMMENT":
      return [...state.filter((comment) => comment.id !== action.payload)];

    case "UPDATE_COMMENT":
      return state.map((comment) => {
        if (comment.id === action.id) {
          const { newComment } = action.data;
          return {
            ...comment,
            comment: newComment,
            editing: false,
          };
        } else return comment;
      });

    default:
      return state;
  }
};
export default commentReducer;
