const initialState = [];

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_POST":
      return [
        ...state,
        {
          postId: new Date().toISOString(),
          ...action.payload,
        },
      ];

    case "EDIT_POST":
      return [
        ...state.map((post) =>
          post.postId === action.payload
            ? {
                ...post,
                editing: !post.editing,
              }
            : post
        ),
      ];

    case "DELETE_POST":
      return [...state.filter((post) => post.postId !== action.payload)];

    case "UPDATE_POST":
      return state.map((post) => {
        if (post.postId === action.postId) {
          const { newContent, updatedDate } = action.data;
          return {
            ...post,
            content: newContent,
            date: updatedDate,
            editing: false,
          };
        } else return post;
      });

    default:
      return state;
  }
};

export default postReducer;
