const initialState = [
  {
    userId: "2020-04-22T03:28:34.381Z",
    name: "User 1",
    userName: "user1",
    email: "user1@email.com",
    phone: "123-456-7890",
  },
];

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_USER":
      return [
        ...state,
        {
          userId: new Date().toISOString(),
          ...action.payload,
        },
      ];

    case "EDIT_USER":
      return [
        ...state.map((user) =>
          user.userId === action.payload
            ? {
                ...user,
                editing: !user.editing,
              }
            : user
        ),
      ];

    case "DELETE_USER":
      return [...state.filter((user) => user.userId !== action.payload)];

    case "UPDATE_USER":
      return state.map((user) => {
        if (user.userId === action.userId) {
          const { newName, newUserName, newEmail, newPhone } = action.data;
          return {
            ...user,
            name: newName,
            userName: newUserName,
            email: newEmail,
            phone: newPhone,
            editing: !user.editing,
          };
        } else return user;
      });

    default:
      return state;
  }
};

export default userReducer;
