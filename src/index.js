import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";

import "bootstrap/dist/css/bootstrap.min.css";
import App from "./App";
import userReducer from "./redux/userReducer";
import postReducer from "./redux/postReducer";
import commentReducer from "./redux/commentReducer";

const store = createStore(
  combineReducers({
    users: userReducer,
    posts: postReducer,
    comments: commentReducer,
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
